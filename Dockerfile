FROM openjdk:19-jdk
ADD ./target/hello-geo-1.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Duser.timezone=Asia/Bangkok","-jar","/app.jar"]
