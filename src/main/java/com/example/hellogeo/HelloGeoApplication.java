package com.example.hellogeo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloGeoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloGeoApplication.class, args);
	}

}
