package com.example.hellogeo.controller;

import com.example.hellogeo.dto.DistanceRequestDto;
import com.example.hellogeo.dto.DistanceResponseDto;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.operation.distance.DistanceOp;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("api/jts")
public class DistanceJTSController {

    @PostMapping("/distance")
    public @ResponseBody DistanceResponseDto distance(@RequestBody DistanceRequestDto distanceRequestDto)
            throws TransformException, FactoryException {

        GeometryFactory gf = JTSFactoryFinder.getGeometryFactory();

        Double[][] polygon = distanceRequestDto.getPolygon();
        Coordinate[] c = new Coordinate[polygon.length+1];
        int i = 0;
        for (Double[] point : polygon) {
            c[i] = new Coordinate(point[1],point[0]);
            i++;
        }
        c[i] = new Coordinate(polygon[0][1],polygon[0][0]);
        Geometry geo = gf.createPolygon(c);

        Double[] point = distanceRequestDto.getPoint();
        Coordinate coordinate = new Coordinate(point[1], point[0]);
        Point p = gf.createPoint(coordinate);

        CoordinateReferenceSystem crs = CRS.decode("EPSG:4326");

        GeodeticCalculator gc = new GeodeticCalculator(crs);
        gc.setStartingPosition( JTS.toDirectPosition(  DistanceOp.nearestPoints(geo, p)[0], crs ) );
        gc.setDestinationPosition( JTS.toDirectPosition( coordinate, crs ) );

        double distance = gc.getOrthodromicDistance();

        return DistanceResponseDto.builder().distance(distance).build();
    }
}
