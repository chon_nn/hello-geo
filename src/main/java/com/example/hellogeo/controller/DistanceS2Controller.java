package com.example.hellogeo.controller;

import com.example.hellogeo.dto.DistanceRequestDto;
import com.example.hellogeo.dto.DistanceResponseDto;
import com.google.common.geometry.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/s2")
public class DistanceS2Controller {

    @PostMapping("distance")
    public @ResponseBody DistanceResponseDto distance(@RequestBody DistanceRequestDto distanceRequestDto){

        List<S2Point> points = new ArrayList<>();

        Double[][] polygonRequest = distanceRequestDto.getPolygon();
        for (Double[] point : polygonRequest) {
            points.add(S2LatLng.fromDegrees(point[1],point[0]).toPoint());
        }
        S2Loop loop = new S2Loop(points);
        S2Polygon polygon = new S2Polygon(loop);

        Double[] pointRequest = distanceRequestDto.getPoint();
        S2Point targetPoint = S2LatLng.fromDegrees(pointRequest[1],pointRequest[0]).toPoint();

        S1Angle s1Angle = polygon.getDistance(targetPoint);

        double distance = s1Angle.e5();

        return DistanceResponseDto.builder().distance(distance).build();
    }
}
