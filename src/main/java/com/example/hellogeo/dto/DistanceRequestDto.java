package com.example.hellogeo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DistanceRequestDto {
    private Double[][] polygon;
    private Double[] point;
}
