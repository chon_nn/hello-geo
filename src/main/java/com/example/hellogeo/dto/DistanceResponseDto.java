package com.example.hellogeo.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DistanceResponseDto {
    private Double distance;
}
